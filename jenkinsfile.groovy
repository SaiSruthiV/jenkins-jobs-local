properties([
    pipelineTriggers([
        [$class: "SCMTrigger", scmpoll_spec: "*/5 * * * *"],
    ])
])

def updateEagleServerUri(uri){
    def inst = Jenkins.getInstance()
    def publish_ssh = inst.getDescriptor("com.mobileenerlytics.eagle.tester.jenkins.eagletesterjenkins.EagleWrapper")
    println(publish_ssh.getEagleServerUri())
    publish_ssh.setEagleServerUri(uri)
    publish_ssh.save()
    println(publish_ssh.getEagleServerUri())
}

node {
    echo "Starting release pipeline"
    def  upstream = currentBuild.rawBuild.getCause(hudson.model.Cause$UpstreamCause)
    def plugins = jenkins.model.Jenkins.instance.pluginManager.activePlugins.findAll {
     ite -> ite.hasUpdate()
    }.collect {
     ite -> ite.getShortName()
    }
    
    println "Plugins to be updated: ${plugins}"
    
    stage ('plugin-update'){
        echo "In plugin update stage"
    if(plugins.contains("eagle-tester")){
    echo "Updating eagle-tester!!"
    echo "Starting plugin update "
    jenkins.model.Jenkins.getInstance().getUpdateCenter().getSites().each { site ->
    site.updateDirectlyNow(hudson.model.DownloadService.signatureCheck)
    }

    hudson.model.DownloadService.Downloadable.all().each { downloadable ->
        downloadable.updateNow();
    }
        long count = 0

    jenkins.model.Jenkins.instance.pluginManager.install(plugins, false).each { f ->
       f.get()
       println "${++count}/${plugins.size()}.."
    }
    echo "Updated plugins!!"
    
   if(plugins.size() != 0 && count == plugins.size()) {
     echo "Restarting Jenkins now."
     jenkins.model.Jenkins.instance.safeRestart()
   }
   
}else{
    echo "Eagle-tester plugin has no updates."
}
}


stage ('server-release'){
    echo "Polling server release"
    git poll: true, url: 'git@bitbucket.org:mobileenerlytics/server.git', branch: 'release'
    if (env.GIT_COMMIT != env.GIT_PREVIOUS_SUCCESSFUL_COMMIT) {
      echo "New updates found in release branch of server repo"
      updateEagleServerUri("https://tester.mobileenerlytics.com")
    }else{
        echo "No updates found in server repo"
    }
}  

stage ('upstream'){
    if(upstream?.shortDescription == "testerapp"){
        echo "Updates found in tester app. New app already installed."
    }
    if(upstream?.shortDescription == "docker"){
        echo "New docker image found. Docker container already started."
        updateEagleServerUri("https://tester.mobileenerlytics.com")
    }
}
    
  echo "End of updates"

    stage ('always'){
        echo "Running base test"
        build 'base-test'
    }
        

}