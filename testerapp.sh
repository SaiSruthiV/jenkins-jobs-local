#!/bin/bash

curl -u deployer:applerules4608 -X GET https://mobileenerlytics.jfrog.io/mobileenerlytics/libs-release-local/com/mobileenerlytics/TesterApp/maven-metadata.xml -o /c/Jenkins/outputtester.xml

latest=($(grep -oP '(?<=latest>)[^<]+' "/c/Jenkins/outputtester.xml"))

echo $latest

curl -u deployer:applerules4608 -X GET https://mobileenerlytics.jfrog.io/mobileenerlytics/libs-release-local/com/mobileenerlytics/TesterApp/$latest/TesterApp-$latest.apk -o testerapp.apk