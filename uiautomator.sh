#!/bin/bash

curl -u deployer:applerules4608 -X GET https://mobileenerlytics.jfrog.io/mobileenerlytics/libs-release-local/com/mobileenerlytics/UiAutomatorTests/maven-metadata.xml -o /c/Jenkins/output.xml

latest=($(grep -oP '(?<=latest>)[^<]+' "/c/Jenkins/output.xml"))

echo $latest

curl -u deployer:applerules4608 -X GET https://mobileenerlytics.jfrog.io/mobileenerlytics/libs-release-local/com/mobileenerlytics/UiAutomatorTests/$latest/UiAutomatorTests-$latest.apk -o UiAutomatorTests-debug.apk
curl -u deployer:applerules4608 -X GET https://mobileenerlytics.jfrog.io/mobileenerlytics/libs-release-local/com/mobileenerlytics/UiAutomatorTests/$latest/UiAutomatorTests-$latest.apk -o UiAutomatorTests-test.apk